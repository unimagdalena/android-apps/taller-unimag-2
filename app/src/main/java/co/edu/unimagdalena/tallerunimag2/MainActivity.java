package co.edu.unimagdalena.tallerunimag2;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout x1, x2, y1, y2;
    Button pendienteButton, cuadranteButton, puntoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        x1 = findViewById(R.id.input_x1);
        x2 = findViewById(R.id.input_x2);
        y1 = findViewById(R.id.input_y1);
        y2 = findViewById(R.id.input_y2);

        puntoButton = findViewById(R.id.punto_medio);
        puntoButton.setOnClickListener(this);
        cuadranteButton = findViewById(R.id.cuadrante);
        cuadranteButton.setOnClickListener(this);
        pendienteButton = findViewById(R.id.pendiente);
        pendienteButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_random:
                FillNumberInputs(-50, 50);
                return true;
            case R.id.action_distance:
                ShowDistance();
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onClick(View view) {
        // Validate inputs
        if (!validateInputs()) {
            Toast.makeText(getApplicationContext(),"Datos inválidos", Toast.LENGTH_LONG).show();
            return;
        }

        // Get values from inputs
        Integer x1Value = Integer.parseInt (x1.getEditText().getText().toString());
        Integer x2Value = Integer.parseInt (x2.getEditText().getText().toString());
        Integer y1Value = Integer.parseInt (y1.getEditText().getText().toString());
        Integer y2Value = Integer.parseInt (y2.getEditText().getText().toString());

        // Handle button selection
        switch (view.getId()) {
            case R.id.punto_medio:
                Integer x = (x1Value+x2Value)/2;
                Integer y = (y1Value+y2Value)/2;
                Toast.makeText(getApplicationContext(),"Punto medio: ("+ x +","+ y +")", Toast.LENGTH_LONG).show();
                break;
            case R.id.pendiente:
                Integer d = x2Value-x1Value;
                if (d != 0) {
                    int m = (y2Value-y1Value)/d;
                    Toast.makeText(getApplicationContext(),"Pendiente: "+ m, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),"Pendiente: "+ d, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.cuadrante:
                Toast.makeText(getApplicationContext(),"(x1, y1): "+getCuadrante(x1Value, y1Value)+" cuadrante " +
                        "(x2, y2 ): "+getCuadrante(x2Value, y2Value)+" cuadrante", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(getApplicationContext(),"Datos inválidos", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private boolean validateInputs() {
        String[] values = new String[4];
        values[0] = x1.getEditText().getText().toString();
        values[1] = x2.getEditText().getText().toString();
        values[2] = y1.getEditText().getText().toString();
        values[3] = y2.getEditText().getText().toString();

        for (String value : values) {
            if (TextUtils.isEmpty(value)) {
                return false;
            }
        }

        return true;
    }

    private void FillNumberInputs(Integer min, Integer max) {
        EditText[] values = new EditText[4];
        values[0] = x1.getEditText();
        values[1] = x2.getEditText();
        values[2] = y1.getEditText();
        values[3] = y2.getEditText();

        for (EditText value : values) {
            Random r = new Random();
            Integer num = r.nextInt((max - min) + 1) + min;

            value.setText(num.toString());
        }

        Toast.makeText(getApplicationContext(),"Puntos generados aleatoriamente", Toast.LENGTH_LONG).show();
    }

    private void ShowDistance() {

        // Get values from inputs
        Integer x1Value = Integer.parseInt (x1.getEditText().getText().toString());
        Integer x2Value = Integer.parseInt (x2.getEditText().getText().toString());
        Integer y1Value = Integer.parseInt (y1.getEditText().getText().toString());
        Integer y2Value = Integer.parseInt (y2.getEditText().getText().toString());

        int a = x2Value - x1Value;
        int b = y2Value - y1Value;
        double d = Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
        double distance = Math.abs(d);

        Toast.makeText(getApplicationContext(),"Distancia: "+ distance, Toast.LENGTH_LONG).show();
    }

    public String getCuadrante(float a, float b){
        if (a > 0 && b > 0) return "Primer";       // Primer cuadrante
        else if (a < 0 && b > 0) return "Segundo";  // Segundo cuadrante
        else if (a < 0 && b < 0) return "Tercer";  // Tercer Cuadrante
        else if (a > 0 && b < 0) return "Cuarto";  // Cuarto cuadrante
        else return "Ningun";
    }
}